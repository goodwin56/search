var TwitterSearch = require('./modules/twitter/twitterSearch.js');



class Search{
    
    constructor(query){
        this.query = query;
        this.searchResults = [];
    }
    
    static run(query){
        return new Search(query).search();
    }
    
    search(){
        var self = this;
        return new Promise(function(resolve, reject){
            TwitterSearch(self.query).then(function(results){
                self.searchResults['twitter'] = results;
                resolve(self.searchResults);
                // return VK.run(query) etc...
            }) //then()...etc;
        });
    }
}

module.exports = Search.run;