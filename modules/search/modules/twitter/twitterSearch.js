var Twitter = require('twitter');

class TwitterSearch {

    constructor(query) {
        this.cur_page = 0;
        this.max_page = 5;
        this.count_per_page = 10;
        
        this.searchResults = [];
        this.client = new Twitter({
            consumer_key: 'kh6cEuwe9zqYrgaEWJeGt5neX',
            consumer_secret: '7fg6wJHT31HVBlc5eEYR599NUggUYRVLhigEZMzL43NyHBt2TC',
            access_token_key: '936235797518409728-Djjd1nJKzg4VKQxO74DnUMKLoBXMx4L',
            access_token_secret: 'VTfzBcAHVrWRYfEZsNk5jSAot27xbTpV9fZIdR7PT03ey'
        });
        this.searchQuery = {
            q: query,
            count: this.count_per_page,
            lang: 'ru',
            max_id: null,
            include_entities: null,
            result_type: 'recent'
        };
    }

    static run(query) {
        return new TwitterSearch(query).search();
    }
    
    /*
     * return null or max_id
     */
    parseUrl(url){
        var regexp = /^\?max_id=(\d*)/,
            str = url,
            result = str.match(regexp);
        return result[1] ? result[1] : null;
    }
    
    addResults(result){
        var self = this;
        result.forEach(function(val,index) {
            console.log('add tweet id: '+val.id);
            console.log('add tweet text: '+val.text);
            console.log('///////////////////////////////////////');
            self.searchResults.push(val);
        })   
    }
    
    getPage(resolve, reject){
        var self = this;
        return self.client.get('search/tweets', self.searchQuery).then(function (tweets) {
            console.log(tweets.search_metadata.next_results);
            console.log("cur page " + self.cur_page);
            if(tweets.search_metadata.next_results){
                self.searchQuery['max_id'] = self.parseUrl(tweets.search_metadata.next_results);
                self.addResults(tweets.statuses);
                self.cur_page++;

                if(self.cur_page >= self.max_page){
                    resolve(self.searchResults);
                }else{
                    console.log("next page " + self.cur_page);
                    return self.getPage(resolve, reject);
                }
                
            }else{
                console.log("no next results stop page " + self.cur_page);
                resolve(self.searchResults);
            }

        })
        .catch(function (error) {
            console.log("error request page " + self.cur_page);
            resolve(self.searchResults);
        });        
    }
    
    search() {
        var self = this;
        return new Promise(function (resolve, reject) {
            return self.getPage(resolve, reject);
        });
    }
}

module.exports = TwitterSearch.run;